# Quick PHP Version Change

# EN
Shell Script for quick PHP version change
# Quick PHP Version Change
 
#PT
Shell Script para troca rÃ¡pida de versÃ£o do PHP
### EN
#### Shell Script for quick PHP version change

### PT
#### Shell Script para troca rápida de versão do PHP

## About
A client uses a very old version of PrestaShop and consequently an old version of PHP.
There came a problem:
I have PHP7.2 installed on my machine (Deepin / Linux) and my client has on their PHP5.6 server;
But to test the system as in my client's current environment it would be necessary to install PHP5.6 on my machine, but I also have other projects that use PHP7 being so I would need to install version 5.6 (no problems) I did, but for switching between one and another would require some commands (kind of boring I must say), so what I did:
I've created some trivial shell scripts to make these changes for me.
Just copy the template script and modify it to the version installed on the system.

As I have 7.2 and 5.6 I have 2:
- One that changes from any current version to 7.2;
- Other that changes from any current version to 5.6;

So I will do:
I will modify the one script for each installed version to only one, which will receive parameters and make the same changes;

Simple, functional and elegant (and in English to be useful in various places);

Soon a video showing how it works ...


## Sobre
Pessoal, peguei um freela outro dia e meu cliente utiliza uma versão bem antiga do PrestaShop e consequentemente uma versão antiga do PHP.
Aí surgiu um problema:
Tenho o PHP7.2 instalada na minha máquina (Deepin/Linux) e meu cliente tem em seu servidor PHP5.6;
Mas para testar o sistema como no ambiente atual do meu cliente seria necesário instalar o PHP5.6 na minha máquina, mas também tenho outros projetos que usam o PHP7 sendo assim seria necessário eu instalar a versão 5.6 (sem problemas) o fiz, mas pra alternar entre um e outro seria necessário alguns comandos (meio chatos devo dizer), então o que eu fiz:
Criei alguns scripts shell apara fazer essas alterações por mim.
Basta copiar o script modelo e modificar para a versão instalada no sistema.

Como tenho o 7.2 e o 5.6 tenho 2:
- Um que muda de qualquer versão atual para o 7.2;
- Outro que muda de qualquer versão atual para o 5.6;

O que farei:
Modificarei o script de um para cada versão instalada para um apenas, o qual receberá parâmetros e fará as mesmas alterações;

Simples, funcional e elegante (e em inglês para ser útil em diversos lugares);

Em breve um vídeo mostrando como funciona...


## How to use

```shell
#!/bin/bash
#
#by Tiago França 10 jan 2018
#contato@tiagofranca.com
#

PHP_PATH="/usr/bin/phpX.x"
CURRENT="php*"
NEW_VERSION="phpX"
NO_ROOT_MSG="Please run as root"
RESTART_Q="service apache2 restart"
```
(...)
_________________________________

## Explaining

```shell
PHP_PATH="/usr/bin/phpX.x"
```
The directory of the PHP version

```shell
CURRENT="php*"
```
The current version static or with * to any others 

```shell
NEW_VERSION="phpX"
```
The new version

```shell
NO_ROOT_MSG="Please run as root"
```
The message if don't root privileges

```shell
RESTART_Q="service apache2 restart"
```
Restart the webserver in this case the apache on Debian based
